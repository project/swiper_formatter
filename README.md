## Swiper formatter

### General Information

Provides Drupal integration with the one of the most modern swiping/sliding libraries [Swiper](https://swiperjs.com/) which is mobile first, layout and gestures wise, and it provides a huge amount
of options for designing your own widget.

This module is not meant to be yet another images slider, or such.
It is rather a swiping UI widget for almost any kind of content,
and in both directions horizontal and vertical - markup/text fields,
media fields (i.e. video), Content entity references fields, Paragraphs,
Views content in the classic way, some type of fields within Views etc.

### Features
- [Swiper's features](https://swiperjs.com/demos) is rather a huge list,
  a solid group of those are included and mostly tested. Note that combinations
  of some particular ones are not meant by [Swiper design](https://swiperjs.com/swiper-api).

### Swiper.js documentation
Please check out [Swiper extensive API](https://swiperjs.com/swiper-api) for more in depth documentation.

### Installation

1. Fetch module via Composer
  `composer require drupal/swiper_formatter`
2. Enable module either via Drupal UI or with Drush
  `drush en swiper_formatter`

### Use
For this and any further info, please check [module's page](https://www.drupal.org/project/swiper_formatter).

#### TODO
- ~~Provide support for Paragraphs~~
- Develop CKEditor 5 plugin

#### Authors/Credits
* [nk_](https://www.drupal.org/u/nk_)
